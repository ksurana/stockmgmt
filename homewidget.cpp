#include "homewidget.h"
#include "ui_homewidget.h"
#include "getquote.h"
#include "updatesymbols.h"
#include "updatescripcodes.h"
#include "jsonparse.h"
extern QSqlDatabase db;
HomeWidget::HomeWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HomeWidget)
{
    ui->setupUi(this);
    proxy = new QSortFilterProxyModel;
    proxy2= new QSortFilterProxyModel;
    createTable();
    createTable2();
    proxy->setSourceModel(model);
    proxy2->setSourceModel(model2);

}

HomeWidget::~HomeWidget()
{
    delete ui;
}

void HomeWidget::createTable()
{
    model = new QSqlTableModel(this,db);
    model->setTable("symbolsnse");
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->select();
    model->setHeaderData(1, Qt::Horizontal, tr("symbols"));
    model->setHeaderData(2, Qt::Horizontal, tr("Company Name"));
    model->setHeaderData(0,Qt::Horizontal, tr("id"));
    ui->tabview->setModel(model);
    ui->tabview->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tabview->resizeColumnsToContents();
}

void HomeWidget::createTable2()
{
    model2 = new QSqlTableModel(this,db);
    model2->setTable("symbolsbse");
    model2->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model2->select();
    model2->setHeaderData(1, Qt::Horizontal, tr("symbols"));
    model2->setHeaderData(2, Qt::Horizontal, tr("Company Name"));
    model2->setHeaderData(0,Qt::Horizontal, tr("id"));
    ui->tabview2->setModel(model2);
    ui->tabview2->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tabview2->resizeColumnsToContents();
    qDebug()<<model2->lastError();
}

void HomeWidget::on_search_textChanged(const QString &arg1)
{

    proxy->setFilterKeyColumn(-1);
    QString find = ui->search->text().trimmed();
    proxy->setFilterWildcard(find);
    ui->tabview->setModel(proxy);
    proxy->setFilterCaseSensitivity(Qt::CaseInsensitive);
}

void HomeWidget::on_tabview_clicked(const QModelIndex &index)
{
    QString price,link,symbol;
    int row = index.row();
    float change;
    symbol = proxy->index(row,0).data().toString();
    link="http://finance.google.com/finance/info?client=ig&q=NSE:"+symbol;
    GetQuote quote;
    link = quote.Do_Download(link);
    qDebug()<<link;
    JSONParse jsonparse(link);
    price = jsonparse.retPrice();
    ui->nseprice->setText(price);
    change = price.toFloat() - jsonparse.retPrevClose().toFloat();
    ui->change->setText(QString("%1").arg(change));
}

void HomeWidget::on_updatesymbol_clicked()
{
    UpdateSymbols *update = new UpdateSymbols();
}

void HomeWidget::on_pushButton_clicked()
{
    UpdateScripCodes *updatescripcodes = new UpdateScripCodes();
}

void HomeWidget::on_search2_textChanged(const QString &arg1)
{
    proxy2->setFilterKeyColumn(-1);
    QString find = ui->search2->text().trimmed();
    proxy2->setFilterWildcard(find);
    ui->tabview2->setModel(proxy2);
    proxy2->setFilterCaseSensitivity(Qt::CaseInsensitive);
}

void HomeWidget::on_tabview2_clicked(const QModelIndex &index)
{
    QString price,link,symbol;
    int row = index.row();
    float change;
    symbol = proxy2->index(row,0).data().toString();
    link="http://finance.google.com/finance/info?client=ig&q=BSE:"+symbol;
    GetQuote quote;
    link = quote.Do_Download(link);
    qDebug()<<link;
    JSONParse jsonparse(link);
    price = jsonparse.retPrice();
    ui->bseprice->setText(price);
    change = price.toFloat() - jsonparse.retPrevClose().toFloat();
    ui->change2->setText(QString("%1").arg(change));
}

void HomeWidget::on_pushButton_2_clicked()
{
    QModelIndexList indexes = ui->tabview->selectionModel()->selectedRows();
    qDebug()<<indexes.count();
    QSqlQuery query(db);
    int i;
    QString symbol,name,cmnd;
    for (i=0;i<indexes.count();i++)
    {
        symbol = proxy->data(proxy->index(indexes.at(i).row(),0)).toString();
        name = proxy->data(proxy->index(indexes.at(i).row(),1)).toString();
        cmnd = QString("Select symbol from watchlistsnse where symbol='%1';").arg(symbol);
        query.exec(cmnd);
        if(query.next())
            qDebug()<<"stock "<<symbol<<" already exists";
        else
        {
            cmnd = QString("Insert into watchnse values('%1','%2',0,0);").arg(symbol).arg(name);
            query.exec(cmnd);
            qDebug()<<query.lastError();
        }
    }
}

void HomeWidget::on_pushButton_3_clicked()
{
    QModelIndexList indexes = ui->tabview2->selectionModel()->selectedRows();
    qDebug()<<indexes.count();
    QSqlQuery query(db);
    int i = 0;
    QString scripcode,name,cmnd;
    for (i;i<indexes.count();i++)
    {
        scripcode = proxy2->data(proxy2->index(indexes.at(i).row(),0)).toString();
        name = proxy2->data(proxy2->index(indexes.at(i).row(),1)).toString();
        cmnd = QString("Select scripcode from watchbse where scripcode='%1';").arg(scripcode);
        query.exec(cmnd);
        if(query.next())
            qDebug()<<"stock "<<scripcode<<" already exists";
        else
        {
            cmnd = QString("Insert into watchbse values('%1','%2',0,0);").arg(scripcode).arg(name);
            query.exec(cmnd);
            qDebug()<<query.lastError();
        }
    }
}
