#include "home.h"
#include <QApplication>
#include <QStyleFactory>
//https://www.mashape.com/drona/stockviz#!endpoint-Live-Price-Equity
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setStyle(QStyleFactory::create("WindowsVista"));
    qDebug()<<QStyleFactory::keys();
    home w;
    w.showMaximized();

    return a.exec();
}
