#-------------------------------------------------
#
# Project created by QtCreator 2014-07-15T22:47:37
#
#-------------------------------------------------
QT       += core gui
QT       += network
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = stockmgmt
TEMPLATE = app


SOURCES += main.cpp\
        home.cpp \
    getquote.cpp \
    homewidget.cpp \
    checkconnection.cpp \
    updatesymbols.cpp \
    updatescripcodes.cpp \
    watchlist.cpp \
    jsonparse.cpp \
    portfolio.cpp \
    addstocktoportfolio.cpp

HEADERS  += home.h \
    getquote.h \
    homewidget.h \
    checkconnection.h \
    updatesymbols.h \
    updatescripcodes.h \
    watchlist.h \
    jsonparse.h \
    portfolio.h \
    addstocktoportfolio.h

FORMS    += home.ui \
    getquote.ui \
    homewidget.ui \
    watchlist.ui \
    portfolio.ui \
    addstocktoportfolio.ui
