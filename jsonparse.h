#ifndef JSONPARSE_H
#define JSONPARSE_H
#include <QMainWindow>
class JSONParse
{
private:
    QString inp;
    int colonIndex(int coloncount);

public:
    JSONParse(QString input="");
    ~JSONParse();
    void setInp(QString input);
    QString retPrice();
    QString retTime();
    QString retPrevClose();

};

#endif // JSONPARSE_H
