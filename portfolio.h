#ifndef PORTFOLIO_H
#define PORTFOLIO_H

#include <QWidget>

namespace Ui {
class Portfolio;
}

class Portfolio : public QWidget
{
    Q_OBJECT

public:
    explicit Portfolio(QWidget *parent = 0);
    ~Portfolio();

private slots:
    void on_addbtn_clicked();

private:
    Ui::Portfolio *ui;
};

#endif // PORTFOLIO_H
