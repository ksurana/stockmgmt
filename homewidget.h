#ifndef HOMEWIDGET_H
#define HOMEWIDGET_H
#include <QtSql>
#include <QWidget>

namespace Ui {
class HomeWidget;
}

class HomeWidget : public QWidget
{
    Q_OBJECT
    QSortFilterProxyModel *proxy,*proxy2;
    QSqlTableModel *model,*model2;
public:
    explicit HomeWidget(QWidget *parent = 0);
    ~HomeWidget();

private slots:
    void createTable();
    void createTable2();
    void on_search_textChanged(const QString &arg1);

    void on_tabview_clicked(const QModelIndex &index);
    
    void on_updatesymbol_clicked();

    void on_pushButton_clicked();

    void on_search2_textChanged(const QString &arg1);

    void on_tabview2_clicked(const QModelIndex &index);

    void on_pushButton_2_clicked();
    
    void on_pushButton_3_clicked();

private:
    Ui::HomeWidget *ui;
};

#endif // HOMEWIDGET_H
