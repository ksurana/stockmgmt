#include "home.h"
#include "ui_home.h"
#include "getquote.h"
#include "homewidget.h"
#include "checkconnection.h"
#include "updatesymbols.h"
#include "watchlist.h"
#include "portfolio.h"
QSqlDatabase db;
QSqlDatabase db2;
home::home(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::home)
{
    ui->setupUi(this);
    db =  QSqlDatabase::addDatabase("QMYSQL","stockMgmt");
    db.setHostName("127.0.0.1");
    db.setPort(3306);
    db.setUserName("root");
    db.setPassword("");
    db.setDatabaseName("stockmgmt");
    db.open();
    qDebug()<<db.lastError();
    if(!(CheckConnection::checkConnection()))
        qApp->quit();
}

home::~home()
{
    delete ui;

}

void home::on_pushButton_clicked()
{
    GetQuote *x=new GetQuote(this);
    x->show();
}

void home::on_actionHome_triggered()
{
    HomeWidget *homewidget = new HomeWidget();
    this->setCentralWidget(homewidget);
}

void home::on_actionWatchList_triggered()
{
    WatchList *watchlist = new WatchList();
    this->setCentralWidget(watchlist);
}

void home::on_actionPortfolio_triggered()
{
    Portfolio *portfolio = new Portfolio();
    this->setCentralWidget(portfolio);
}
