#ifndef ADDSTOCKTOPORTFOLIO_H
#define ADDSTOCKTOPORTFOLIO_H
#include <QStandardItemModel>
#include <QDialog>
#include <QtSql>
namespace Ui {
class AddStockToPortfolio;
}

class AddStockToPortfolio : public QDialog
{
    Q_OBJECT

public:
    explicit AddStockToPortfolio(QWidget *parent = 0);
    ~AddStockToPortfolio();

private slots:
    void on_search_textChanged(const QString &arg1);

    void on_search2_textChanged(const QString &arg1);

    void on_pushButton_clicked();

    void disableBse();

    void disableNse();

private:
    void loadCb1();
    void loadCb2();
    QStandardItemModel *cbmodel1;
    QStandardItemModel *cbmodel2;
    QSortFilterProxyModel *proxy,*proxy2;
    Ui::AddStockToPortfolio *ui;
};

#endif // ADDSTOCKTOPORTFOLIO_H
