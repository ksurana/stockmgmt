#ifndef EXPORTMODEL_H
#define EXPORTMODEL_H
#include <QtSql>
#include "xlsxdocument.h"

class ExportModel
{
public:
    ExportModel();
    static void createFileFromModel(QSqlTableModel *model);
};

#endif // EXPORTMODEL_H
