#ifndef GETQUOTE_H
#define GETQUOTE_H

#include <QWidget>
#include <QNetworkAccessManager>
#include <QEventLoop>
namespace Ui {
class GetQuote;
}

class GetQuote : public QWidget
{
    Q_OBJECT

public:
    explicit GetQuote(QWidget *parent = 0);
    ~GetQuote();
    QString Do_Download(QString link="");
private slots:
    void updateProgress(qint64 recieved, qint64 total);
    void writeData();


    void on_pushButton_clicked();

private:
    Ui::GetQuote *ui;
};

#endif // GETQUOTE_H
