This is a private repository.

**About Software**
A simple lightweight stock management / portfolio management software which uses google finance api to get stock quotes.

**Features**

* Get instantaneous stock quote.
* Supports NSE and BSE
* You can create watchlist of your favorite stocks.
* Create Portfolio
* Make transactions (like buy / sell stock )
* Report Generation. Specify dates and get report of your stocks ( net profit , loss, avg price etc of the stock)
* Stock tracker. Set a price and this software will notify you as soon as the stock reaches the specified price.