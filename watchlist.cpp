#include "watchlist.h"
#include "ui_watchlist.h"
#include "getquote.h"
#include "jsonparse.h"
extern QSqlDatabase db;
WatchList::WatchList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WatchList)
{
    ui->setupUi(this);
    createTable1();
    createTable2();
    refreshPrice();
    proxy = new QSortFilterProxyModel;
    proxy2= new QSortFilterProxyModel;
    proxy->setSourceModel(model);
    proxy2->setSourceModel(model2);
}

WatchList::~WatchList()
{
    delete ui;
}

void WatchList::on_pushButton_3_clicked()
{
    QString cmnd,scripcode;
    QSqlQuery query(db);
    QMessageBox *msgBox = new QMessageBox();
    ui->tabview2->setEditTriggers(QAbstractItemView::AllEditTriggers);
    QModelIndexList index = ui->tabview2->selectionModel()->selectedRows();
    if(index.count()==0)
    {
        msgBox->setText("Select a row to delete");
        msgBox->setStandardButtons(QMessageBox::Ok);
        msgBox->exec();
    }
    else
    {
        //if(!(CheckConnection::checkConnection()))
        //    qApp->quit();
        for (int i=0;i<index.count();i++)
        {
            scripcode=proxy2->data(proxy2->index(index.at(i).row(),0)).toString();
            cmnd=QString("Delete from watchbse where scripcode='%1';").arg(scripcode);
            query.exec(cmnd);
            qDebug()<<query.lastError();
        }
        //model->removeRows(index.first().row(),index.count());
        model2->submitAll();
        //emit Status("Deleted");
    }
    ui->tabview2->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

void WatchList::createTable1()
{
    model = new QSqlTableModel(this,db);
    model->setTable("watchnse");
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->select();
    model->setHeaderData(0, Qt::Horizontal, tr("symbols"));
    model->setHeaderData(1, Qt::Horizontal, tr("Company Name"));
    model->setHeaderData(2,Qt::Horizontal, tr("Price"));
    model->setHeaderData(3,Qt::Horizontal, tr("Changed"));
    ui->tabview1->setModel(model);
    ui->tabview1->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tabview1->resizeColumnsToContents();
}

void WatchList::createTable2()
{
    model2 = new QSqlTableModel(this,db);
    model2->setTable("watchbse");
    model2->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model2->select();
    model2->setHeaderData(1, Qt::Horizontal, tr("Scrip Code"));
    model2->setHeaderData(2, Qt::Horizontal, tr("Company Name"));
    model2->setHeaderData(3,Qt::Horizontal, tr("Price"));
    model2->setHeaderData(3,Qt::Horizontal, tr("Changed"));
    ui->tabview2->setModel(model2);
    ui->tabview2->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tabview2->resizeColumnsToContents();
}

void WatchList::refreshPrice()
{
    //for nse;
    QSqlQuery query(db);
    int i,rows=model->rowCount();
    float change;
    QString symbol,link,price,cmnd;
    GetQuote quote;
    JSONParse jsonparse;
    for(i=0;i<rows;i++)
    {
        symbol = model->data(model->index(i,0)).toString();
        link="http://finance.google.com/finance/info?client=ig&q=NSE:"+symbol;
        link = quote.Do_Download(link);
        jsonparse.setInp(link);
        price = jsonparse.retPrice();
        qDebug()<<price;
        change = price.toFloat() - jsonparse.retPrevClose().toFloat();
        cmnd = QString("UPDATE watchnse SET price=%1,changed=%2 where symbol='%3';").arg(price).arg(change).arg(symbol);
        query.exec(cmnd);
    }
    // for bse
    rows=model2->rowCount();
    for(i=0;i<rows;i++)
    {
        symbol = model2->data(model2->index(i,0)).toString();
        link="http://finance.google.com/finance/info?client=ig&q=BSE:"+symbol;
        link = quote.Do_Download(link);
        jsonparse.setInp(link);
        price = jsonparse.retPrice();
        qDebug()<<price;
        change = price.toFloat() - jsonparse.retPrevClose().toFloat();
        cmnd = QString("UPDATE watchbse SET price=%1,changed=%2 where scripcode='%3';").arg(price).arg(change).arg(symbol);
        query.exec(cmnd);
    }
    model->submitAll();
    model2->submitAll();
}

void WatchList::on_pushButton_2_clicked()
{
    QString symbol,cmnd;
    QSqlQuery query(db);
    QMessageBox *msgBox = new QMessageBox();
    ui->tabview1->setEditTriggers(QAbstractItemView::AllEditTriggers);
    QModelIndexList index = ui->tabview1->selectionModel()->selectedRows();
    if(index.count()==0)
    {
        msgBox->setText("Select a row to delete");
        msgBox->setStandardButtons(QMessageBox::Ok);
        msgBox->exec();
    }
    else
    {
        //if(!(CheckConnection::checkConnection()))
        //    qApp->quit();
        for (int i=0;i<index.count();i++)
        {
            symbol=proxy->data(proxy->index(index.at(i).row(),0)).toString();
            cmnd=QString("Delete from watchnse where symbol='%1';").arg(symbol);
            query.exec(cmnd);
        }

        //model->removeRows(index.first().row(),index.count());
        model->submitAll();
        //emit Status("Deleted");
    }
    ui->tabview1->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

void WatchList::on_pushButton_clicked()
{
    this->refreshPrice();
}

void WatchList::on_search_textChanged(const QString &arg1)
{
    proxy->setFilterKeyColumn(-1);
    QString find = ui->search->text().trimmed();
    proxy->setFilterWildcard(find);
    ui->tabview1->setModel(proxy);
    proxy->setFilterCaseSensitivity(Qt::CaseInsensitive);
}

void WatchList::on_search2_cursorPositionChanged(int arg1, int arg2)
{
    proxy2->setFilterKeyColumn(-1);
    QString find = ui->search2->text().trimmed();
    proxy2->setFilterWildcard(find);
    ui->tabview2->setModel(proxy2);
    proxy2->setFilterCaseSensitivity(Qt::CaseInsensitive);
}

