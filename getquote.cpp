#include "getquote.h"
#include "ui_getquote.h"
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>
QString ret;
QEventLoop eventLoop;
GetQuote::GetQuote(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GetQuote)
{
    ui->setupUi(this);
}

QString GetQuote::Do_Download(QString link)
{
    //QEventLoop eventLoop;
    QNetworkAccessManager *netManager = new QNetworkAccessManager(this);
    QUrl url(link);
    QNetworkRequest req(url);
    QNetworkReply *reply = netManager->get(req);

    connect(reply,SIGNAL(finished()),this,SLOT(writeData()));
    //connect(reply,SIGNAL(downloadProgress(qint64,qint64)),this,SLOT(updateProgress(qint64,qint64)));
    eventLoop.exec();
    qDebug()<<"completed downloading and writeing.";
    return ret;
    //writeData(reply);
}

void GetQuote::updateProgress(qint64 recieved,qint64 total)
{
    int progress = (100*recieved)/total;
    qDebug()<<recieved;
    ui->progressBar->setValue(progress);
}

void GetQuote::writeData()
{

    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    qDebug()<<"reached";
    if (reply)
    {
        if (reply->error() == QNetworkReply::NoError)
        {
            QFile file("myfile.txt");
            if(file.open(QIODevice::WriteOnly))
            {
                    //file.write(reply->readAll());
                ret = reply->readAll();
                eventLoop.exit();
            }
            else
            {
                ret = "Error writing downloaded file for mirror installation";
                eventLoop.exit();
            }
        }
        else
        {
            //get http status code
            int httpStatus = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
            ret = "HTTP Error code while downloading from mirror: "+httpStatus;
            eventLoop.exit();
        }

        reply->deleteLater();
    }
    else
    {
        ret ="Error downloading file from installation mirror";
        eventLoop.exit();
    }

}

GetQuote::~GetQuote()
{
    delete ui;
}

void GetQuote::on_pushButton_clicked()
{
    QString link = ui->url->text();
    this->Do_Download(link);;
}
