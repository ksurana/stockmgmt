#include "checkconnection.h"
#include <QtSql>
#include <QMessageBox>

extern QSqlDatabase db;
extern QSqlDatabase db2;

CheckConnection::CheckConnection()
{
}

bool CheckConnection::checkConnection(int index)
{
    QMessageBox msgBox;
    db.close();
    bool open;
    if (index = 1)
        open = db.open();
    else
        open = db2.open();
    while (!open)
    {
        msgBox.setText("Connection error");
        msgBox.setStandardButtons(QMessageBox::Retry | QMessageBox::Cancel);
        int ret = msgBox.exec();
        if(ret ==  QMessageBox::Retry)
            open = db.open();
        else
            return false;
    }
    return true;
}
