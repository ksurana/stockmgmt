#ifndef HOME_H
#define HOME_H

#include <QMainWindow>
#include <QtSql>
namespace Ui {
class home;
}

class home : public QMainWindow
{
    Q_OBJECT

public:
    explicit home(QWidget *parent = 0);
    ~home();

private slots:
    void on_pushButton_clicked();

    void on_actionHome_triggered();

    void on_actionWatchList_triggered();

    void on_actionPortfolio_triggered();

private:
    Ui::home *ui;
};

#endif // HOME_H
