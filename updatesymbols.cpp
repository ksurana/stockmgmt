#include "updatesymbols.h"
#include <QFile>
#include <QDebug>
#include <QtSql>
extern QSqlDatabase db;
UpdateSymbols::UpdateSymbols()
{
    QSqlQuery query(db);
    QString cmnd;
    cmnd="DELETE FROM `symbolsnse` WHERE 1";
    query.exec(cmnd);
    qDebug()<<query.lastError();
    int count=0,i;
    QString symbol,name;
    QFile fp("EQUITY_L.csv");
    if(fp.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        while (!fp.atEnd())
        {
            i=0;symbol="";name="";
            QString line = fp.readLine();
            while(line.at(i)!=',')
            {
                symbol=symbol+line.at(i);
                i++;
            }
            i++;
            while(line.at(i)!=',')
            {
                name+=line.at(i);
                i++;
            }
            count++;
            this->insert(symbol,name);
        }
    }
    qDebug()<<count;
}

void UpdateSymbols::insert(QString symbol,QString name)
{
    QString cmnd=QString("Insert into symbolsnse values('%1','%2');").arg(symbol).arg(name);
    QSqlQuery query(db);
    query.exec(cmnd);
}

