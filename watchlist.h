#ifndef WATCHLIST_H
#define WATCHLIST_H

#include <QWidget>
#include <QtSql>
#include <QMessageBox>
namespace Ui {
class WatchList;
}

class WatchList : public QWidget
{
    Q_OBJECT
    QSortFilterProxyModel *proxy,*proxy2;
    QSqlTableModel *model,*model2;
public:
    explicit WatchList(QWidget *parent = 0);
    ~WatchList();

private slots:
    void on_pushButton_3_clicked();
    void createTable1();
    void createTable2();
    void refreshPrice();

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_search_textChanged(const QString &arg1);

    void on_search2_cursorPositionChanged(int arg1, int arg2);



private:
    Ui::WatchList *ui;
};

#endif // WATCHLIST_H
