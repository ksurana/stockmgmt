#include "addstocktoportfolio.h"
#include "ui_addstocktoportfolio.h"
#include <QStandardItemModel>
#include "getquote.h"
#include "jsonparse.h"
extern QSqlDatabase db;
AddStockToPortfolio::AddStockToPortfolio(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddStockToPortfolio)
{
    ui->setupUi(this);
    loadCb1();
    loadCb2();
    proxy = new QSortFilterProxyModel;
    proxy2= new QSortFilterProxyModel;
    proxy->setSourceModel(cbmodel1);
    proxy2->setSourceModel(cbmodel2);
    connect(ui->radioButton,SIGNAL(clicked()),this,SLOT(disableBse()));
    connect(ui->radioButton_2,SIGNAL(clicked()),this,SLOT(disableNse()));
}

AddStockToPortfolio::~AddStockToPortfolio()
{
    delete ui;
}
void AddStockToPortfolio::loadCb1()
{
    QSqlQuery query(db);
    QString cmnd="SELECT name from symbolsnse";
    query.exec(cmnd);
    cbmodel1 =  new QStandardItemModel();
    while (query.next())
    {
        cbmodel1->appendRow(new QStandardItem(query.value(0).toString()));
    }
    ui->cb1->setModel(cbmodel1);
}

void AddStockToPortfolio::loadCb2()
{
    QSqlQuery query(db);
    QString cmnd="SELECT name from symbolsbse";
    query.exec(cmnd);
    cbmodel2 =  new QStandardItemModel();
    while (query.next())
    {
        cbmodel2->appendRow(new QStandardItem(query.value(0).toString()));
    }
    ui->cb2->setModel(cbmodel2);
}

void AddStockToPortfolio::on_search_textChanged(const QString &arg1)
{
    proxy->setFilterKeyColumn(-1);
    QString find = ui->search->text().trimmed();
    proxy->setFilterWildcard(find);
    ui->cb1->setModel(proxy);
    proxy->setFilterCaseSensitivity(Qt::CaseInsensitive);
}

void AddStockToPortfolio::on_search2_textChanged(const QString &arg1)
{
    proxy2->setFilterKeyColumn(-1);
    QString find = ui->search2->text().trimmed();
    proxy2->setFilterWildcard(find);
    ui->cb2->setModel(proxy2);
    proxy2->setFilterCaseSensitivity(Qt::CaseInsensitive);
}

void AddStockToPortfolio::on_pushButton_clicked()
{
    QSqlQuery query(db);
    QString exchange,name,qty,avg,date,investment,marketprice,currentworth,qtysold,sellavg,profit,cmnd,stockno,symbol;
    cmnd="SELECT stockno from portfolio ORDER BY stockno DESC;";
    query.exec(cmnd);
    if(query.next())
        stockno=query.value(0).toString()+1;
    else
        stockno="0";
    if (ui->radioButton->isChecked())
    {
        exchange="nse";
        name=ui->cb1->currentText();
        cmnd = QString("Select symbol from symbolsnse WHERE name='%1';").arg(name);
        query.exec(cmnd);
        query.next();
        symbol=query.value(0).toString();
        GetQuote quote;
        JSONParse jsonparse;
        QString link="http://finance.google.com/finance/info?client=ig&q=NSE:"+symbol;
        link = quote.Do_Download(link);
        jsonparse.setInp(link);
        marketprice = jsonparse.retPrice();
    }
    else
    {
        exchange="bse";
        name=ui->cb2->currentText();
        cmnd = QString("Select symbol from symbolsbse WHERE name='%1';").arg(name);
        query.exec(cmnd);
        query.next();
        symbol=query.value(0).toString();
        GetQuote quote;
        JSONParse jsonparse;
        QString link="http://finance.google.com/finance/info?client=ig&q=BSE:"+symbol;
        link = quote.Do_Download(link);
        jsonparse.setInp(link);
        marketprice = jsonparse.retPrice();
    }
    qty=ui->lineEdit->text();
    avg=ui->lineEdit_3->text();
    date=ui->dateEdit->text();
    int temp = qty.toInt()*avg.toFloat();
    investment = QString("%1").arg(temp);
    currentworth="0";
    qtysold="0";
    sellavg="0";
    temp = (marketprice.toFloat()-avg.toFloat())*qty.toInt();
    profit=QString("%1").arg(temp);
    cmnd=QString("Insert into portfolio values('%1','%2','%3','%4','%5','%6','%7','%8','%9','%10','%11','%12');").arg(stockno).arg(name).arg(exchange).arg(qty).arg(avg).arg(investment).arg(marketprice).arg(currentworth).arg(qtysold).arg(sellavg).arg(profit).arg(date);
    query.exec(cmnd);
    qDebug()<<query.lastError();
    qDebug()<<cmnd;
}

void AddStockToPortfolio::disableBse()
{
    ui->search->setEnabled(true);
    ui->cb1->setEnabled(true);
    ui->search2->setEnabled(false);
    ui->cb2->setEnabled(false);
}

void AddStockToPortfolio::disableNse()
{
    ui->search->setEnabled(false);
    ui->cb1->setEnabled(false);
    ui->search2->setEnabled(true);
    ui->cb2->setEnabled(true);
}
