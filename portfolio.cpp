#include "portfolio.h"
#include "ui_portfolio.h"
#include "addstocktoportfolio.h"
Portfolio::Portfolio(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Portfolio)
{
    ui->setupUi(this);
}

Portfolio::~Portfolio()
{
    delete ui;
}

void Portfolio::on_addbtn_clicked()
{
    AddStockToPortfolio *add = new AddStockToPortfolio();
    add->show();
}
